/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aulawebservicecliente;

/**
 *
 * @author Tiago Arruda
 */
public class AulaWebserviceCliente {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Soma = " + soma(17, 9));
    }

    private static int soma(int arg0, int arg1) {
        webservice.OperacaoImplService service = new webservice.OperacaoImplService();
        webservice.Operacao port = service.getOperacaoImplPort();
        return port.soma(arg0, arg1);
    }

}
