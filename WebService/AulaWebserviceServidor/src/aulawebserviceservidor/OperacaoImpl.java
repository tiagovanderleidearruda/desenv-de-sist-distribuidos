/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aulawebserviceservidor;

import javax.jws.WebService;

/**
 *
 * @author Tiago Arruda
 */
@WebService(endpointInterface = "aulawebserviceservidor.Operacao")
public class OperacaoImpl implements Operacao{

    @Override
    public int soma(int a, int b) {
        return a + b;
    }
}
