/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author Tiago Arruda
 */
public interface CpfInterface extends Remote{
    boolean verifica_cpf(String cpf) throws RemoteException;
}
