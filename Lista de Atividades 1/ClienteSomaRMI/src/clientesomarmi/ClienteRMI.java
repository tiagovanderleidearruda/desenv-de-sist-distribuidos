/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientesomarmi;

import interfaces.CpfInterface;
import interfaces.EquacaoInterface;
import interfaces.Operacoes;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

/**
 *
 * @author Tiago Arruda
 */
public class ClienteRMI {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        Registry registro;
 
        try {
            
            System.out.println("Informe a operação desejada: \n\n(a)Soma;\n(b)Verifica CPF;\n(c)Equação y = x² + 4x - 2;\n\n");
            String ope = entrada.next();
            
            if (ope.equals("a")) {
                System.out.println("Informe o valor para a:\n");
                int a = Integer.parseInt(entrada.next());
            
                System.out.println("Informe o valor para b:\n");
                int b = Integer.parseInt(entrada.next());
                
                registro = LocateRegistry.getRegistry("127.0.0.1", 2099);
                Operacoes operacao = (Operacoes)registro.lookup("Operacao");
                System.err.println("\n\nSoma = " + operacao.soma(a, b));       
            } else
            if (ope.equals("b")) {
                System.out.println("Informe o cpf:\n");
                String cpf = entrada.next();
                
                registro = LocateRegistry.getRegistry("127.0.0.1", 2098);
                CpfInterface cpf_impl = (CpfInterface)registro.lookup("CPF");
                System.err.println("\n\nCPF " + (cpf_impl.verifica_cpf(cpf)?"é valido!":"não é válido!")); 
            } else
            if(ope.equals("c")) {
                System.out.println("Informe o valor de x:\n");
                int x = Integer.parseInt(entrada.next());
                
                registro = LocateRegistry.getRegistry("127.0.0.1", 2097);
                EquacaoInterface equacao_impl = (EquacaoInterface)registro.lookup("Equacao");
                System.err.println("\n\ny = " + equacao_impl.equacao_2grau(x));  
            } else
                System.err.println("Operação informada inválida!");
            
 
        } catch(Exception ex) {
            System.err.println("Erro: " + ex.toString());
        }
    }
}
