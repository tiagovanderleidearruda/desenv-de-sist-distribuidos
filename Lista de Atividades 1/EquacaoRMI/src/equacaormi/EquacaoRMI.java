/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package equacaormi;

import interfaces.EquacaoInterface;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author Tiago Arruda
 */
public class EquacaoRMI implements EquacaoInterface{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try {
            /* REGISTRA O SERVICO NA PORTA 1099 */
            Registry registro = LocateRegistry.createRegistry(2097);

            EquacaoRMI equacao = new EquacaoRMI();

            EquacaoInterface operacao = (EquacaoInterface) UnicastRemoteObject.exportObject(equacao, 0);

            registro.bind("Equacao", operacao);

            System.err.println("Em execução");
            
        } catch (Exception ex) {
            System.err.println("Erro: " + ex.getMessage());
            ex.printStackTrace();
        }
    
    }

    @Override
    public int equacao_2grau(int x) throws RemoteException {
        int y = x * x + 4 * x - 2;
        return y;
    }
}
