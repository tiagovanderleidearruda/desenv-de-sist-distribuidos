/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cpfrmi;

import interfaces.CpfInterface;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author Tiago Arruda
 */
public class CpfRMI implements CpfInterface{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try {
            /* REGISTRA O SERVICO NA PORTA 1099 */
            Registry registro = LocateRegistry.createRegistry(2098);

            CpfRMI cpf = new CpfRMI();

            CpfInterface operacao = (CpfInterface) UnicastRemoteObject.exportObject(cpf, 0);

            registro.bind("CPF", operacao);

            System.err.println("Em execução");
            
        } catch (Exception ex) {
            System.err.println("Erro: " + ex.getMessage());
            ex.printStackTrace();
        }
    
    }

    @Override
    public boolean verifica_cpf(String cpf) throws RemoteException {
        ValidaCPF validacpf = new ValidaCPF();
        return validacpf.isCPF(cpf);
    }
}
