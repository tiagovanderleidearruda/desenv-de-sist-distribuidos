package VerificaCPFApp;


/**
* VerificaCPFApp/ValidacaoOperations.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from ./VerificaCPFApp.idl
* Sexta-feira, 20 de Setembro de 2019 00h41min48s BRT
*/

public interface ValidacaoOperations 
{
  int verifica_cpf (int cpf);
  void shutdown ();
} // interface ValidacaoOperations
