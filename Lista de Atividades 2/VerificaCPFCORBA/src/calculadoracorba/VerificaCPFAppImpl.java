package calculadoracorba;

import VerificaCPFApp.ValidacaoPOA;
import org.omg.CORBA.ORB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author TIAGO ARRUDA
 */
public class VerificaCPFAppImpl extends ValidacaoPOA {

    private ORB orb;

    public void setORB(ORB orb_val) {
        orb = orb_val;
    }

    @Override
    public void shutdown() {
         orb.shutdown(false);
    }

    @Override
    public int verifica_cpf(int cpf) {
        ValidaCPF validacpf = new ValidaCPF();
        return validacpf.isCPF(cpf)?1:0;
    }
}
