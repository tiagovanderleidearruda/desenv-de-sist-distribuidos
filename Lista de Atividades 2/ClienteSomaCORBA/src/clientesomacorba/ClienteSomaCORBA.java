/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientesomacorba;

import CalculadoraApp.*;
import VerificaCPFApp.Validacao;
import VerificaCPFApp.ValidacaoHelper;
import org.omg.CosNaming.*;
import org.omg.CORBA.*;

/**
 *
 * @author Tiago Arruda
 */
public class ClienteSomaCORBA {
    static Calculadora calculadora;
    static Validacao validacao;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // Cria e inicializa o ORB
            ORB orb = ORB.init(args, null);

            // obtem referencia do serviço, com nome "NameService"
            org.omg.CORBA.Object objRef
                    = orb.resolve_initial_references("NameService");
            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

            // Recupera a referência do objeto nomeado ("Calculadora")
            String name = "Calculadora";
            calculadora = CalculadoraHelper.narrow(ncRef.resolve_str(name));

            validacao = ValidacaoHelper.narrow(ncRef.resolve_str("Validacao"));

            /* Invoca método sayHello(), do objeto remoto */
            //System.out.println(calculadora.soma(5, 10, 3));
            System.out.println(calculadora.equacao(5));

            //System.out.println(validacao.verifica_cpf(326827458));
            
            
            
            // Finaliza servidor CORBA
            //calculadora.shutdown();

        } catch (Exception e) {
            System.out.println("ERROR : " + e);
            e.printStackTrace(System.out);
        }
    }
    
}
