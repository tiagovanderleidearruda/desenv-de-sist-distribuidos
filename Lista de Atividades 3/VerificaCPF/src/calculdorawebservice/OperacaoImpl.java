/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculdorawebservice;

import cpfrmi.ValidaCPF;
import javax.jws.WebService;

/**
 *
 * @author Tiago Arruda
 */
@WebService(endpointInterface = "calculdorawebservice.Operacao")
public class OperacaoImpl implements Operacao{
    @Override
    public boolean verificaCpf(String cpf) {
        return ValidaCPF.isCPF(cpf);
    }
}
