/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientesomarmi;

import java.util.Scanner;

/**
 *
 * @author Tiago Arruda
 */
public class ClienteRMI {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
 
        try {
            
            System.out.println("Informe a operação desejada: \n\n(a)Soma;\n(b)Verifica CPF;\n(c)Equação y = x² + 4x - 2;\n\n");
            String ope = entrada.next();
            
            if (ope.equals("a")) {
                System.out.println("Informe o valor para a:\n");
                int a = Integer.parseInt(entrada.next());
            
                System.out.println("Informe o valor para b:\n");
                int b = Integer.parseInt(entrada.next());
                
                System.err.println("\n\nSoma = " + soma(a, b));       
            } else
            if (ope.equals("b")) {
                System.out.println("Informe o cpf:\n");
                String cpf = entrada.next();
                
                System.err.println("\n\nCPF " + (verificaCpf(cpf)?"é valido!":"não é válido!")); 
            } else
            if(ope.equals("c")) {
                System.out.println("Informe o valor de x:\n");
                int x = Integer.parseInt(entrada.next());
                
                System.err.println("\n\ny = " + equacao(x));  
            } else
                System.err.println("Operação informada inválida!");
            
 
        } catch(Exception ex) {
            System.err.println("Erro: " + ex.toString());
        }
    }

    private static boolean verificaCpf(java.lang.String arg0) {
        webservice_cpf.OperacaoImplService service = new webservice_cpf.OperacaoImplService();
        webservice_cpf.Operacao port = service.getOperacaoImplPort();
        return port.verificaCpf(arg0);
    }

    private static int equacao(int arg0) {
        webservice_calculadora.OperacaoImplService service = new webservice_calculadora.OperacaoImplService();
        webservice_calculadora.Operacao port = service.getOperacaoImplPort();
        return port.equacao(arg0);
    }

    private static int soma(int arg0, int arg1) {
        webservice_calculadora.OperacaoImplService service = new webservice_calculadora.OperacaoImplService();
        webservice_calculadora.Operacao port = service.getOperacaoImplPort();
        return port.soma(arg0, arg1);
    }
    
}
