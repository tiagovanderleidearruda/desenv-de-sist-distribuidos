/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculdorawebservice;

import javax.jws.WebService;

/**
 *
 * @author Tiago Arruda
 */
@WebService(endpointInterface = "calculdorawebservice.Operacao")
public class OperacaoImpl implements Operacao{

    @Override
    public int soma(int a, int b) {
        return a + b;
    }

    @Override
    public int equacao(int x) {
        // y = x² + 4x - 2
        return x*x + 4*x - 2;
    }
}
