/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculdorawebservice;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 *
 * @author Tiago Arruda
 */
@WebService
@SOAPBinding(style=SOAPBinding.Style.RPC)
public interface Operacao {
    @WebMethod int soma(int a, int b);
    @WebMethod int equacao(int x);
}