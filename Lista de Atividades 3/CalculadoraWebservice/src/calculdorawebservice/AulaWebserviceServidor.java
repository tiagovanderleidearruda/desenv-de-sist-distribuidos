/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculdorawebservice;

import javax.xml.ws.Endpoint;

/**
 *
 * @author Tiago Arruda
 */
public class AulaWebserviceServidor {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Endpoint.publish("http://localhost:9000/aplicacao", new OperacaoImpl());
    }
}
